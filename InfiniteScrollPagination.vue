<template>
  <div>
    <LoadPrevContent
      :showLoadPrevContent="!!prevPage"
      :isPrevContentLoading="loading.prev"
      @fetchPrevContent="fetchPrevContent(prevPage, 'prev')"
    />
    <!-- Mutation observer is used to detect slot changes aka parent component mounted to dom -->
    <div class="mutation-observer">
      <slot :dataTarget="dataTarget" />
    </div>

    <LoadNextContent
      :showLoadNextContent="!!nextPage"
      :isNextContentLoading="loading.next"
      :useLoadMoreButton="useLoadMoreButton"
      @fetchNextContent="fetchMoreContent(nextPage, 'next')"
    />
  </div>
</template>
<script lang="ts">
import { Component, Vue, Prop, Action } from 'nuxt-property-decorator';
import { generateLinkTagsData } from '~/common/helpers/common';
import { Loading, Pagination } from '~/common/types';
import LoadPrevContent from '~/common/components/ContentLoading/LoadPrevContent.vue';
import LoadNextContent from '~/common/components/ContentLoading/LoadNextContent.vue';

/* =========================================================================
  Please keep populating below validators if you add new modules or apiProps
  ========================================================================== */

const vuexModuleValidator = {
  movies: {
    moviesNew: true,
    bhsMoviesNew: true,
  },
  series: {
    seriesNew: true,
  },
  persons: {
    directorsNew: true,
    performersNew: true,
    guestStarsNew: true,
    couplesNew: true,
    photographersNew: true,
    artistsNew: true,
  },
  tags: {
    tagsNew: true,
  },
  products: {},
  confessions: {
    confessionsNew: true,
  },
};

const apiProps = [
  'getMovies',
  'getBhsMovies',
  'getDirectors',
  'getPerformers',
  'getGuestStars',
  'getCouples',
  'getPhotographers',
  'getArtists',
  'getTags',
  'getConfessions',
  'getSeries',
];

@Component({
  components: {
    LoadPrevContent, // dynamic import causes hydration errors for this component
    LoadNextContent, // dynamic import causes hydration errors for this component
  },
})
export default class ContentWrapper extends Vue {
  @Prop({ default: false }) useLoadMoreButton: boolean;
  @Prop({
    default: () => ({
      prev: false,
      next: false,
    }),
    required: true,
  })
  loading: Loading;
  @Prop({ required: true }) totalPages: number;
  @Prop({
    required: true,
    validator: ([vuexModule, vuexModuleProp]) => {
      return vuexModuleValidator[vuexModule][vuexModuleProp];
    },
  })
  vuexModulesForFetch: string[];
  @Prop({
    required: true,
    validator: (apiProp) => {
      return apiProps.includes(apiProp);
    },
  })
  apiProp: string;

  @Prop({
    required: true,
    default: () => ({
      title: 'please povide meta title',
      description: 'please provide meta description',
    }),
  })
  metaData: {
    title: string;
    description: string;
  };

  @Action('logic/setPagination') setPagination: (paginationOptions: Pagination) => void;

  dataTarget = 'scrollableContent';
  pageItemsNodeList: NodeList = null;

  currentPage: number = 0;
  prevPage: number = null;
  nextPage: number = null;

  observer: IntersectionObserver = null;
  observerConfig = {
    root: null,
    rootMargin: '-80px 0px', // inverting rootMargin top, bottom for approx height of navbar and bottom bar.
    threshold: 0.05, // at least 5% of the element should be in observer viewport to trigger intersection
  };
  mutationObserver: MutationObserver = null; // mutation observer is used to detect slot changes / parent component mounted to dom or newly added pages content

  initiateUrlObserver() {
    this.observer = new IntersectionObserver(([{ isIntersecting, target }]) => {
      if (isIntersecting) {
        // page=1 should not exist
        const urlPageQuery = target.getAttribute('data-url').replace(/\?page=1\b/, '');
        const replaceUrl = `${this.$route.path}${urlPageQuery}`;
        // Making sure that we are not replacing same route

        if (replaceUrl !== this.$route.fullPath) this.$router.replace({ path: replaceUrl });
        // getting current page to update pagination component while observer changes the page. + converts [number] to number
        const newCurrentPage = +urlPageQuery.match(/\d+/);
        this.controlPagination('update', newCurrentPage);
      }
    }, this.observerConfig);
  }

  initiateMutationObserver() {
    const mutationObserverTargetEl = document.getElementsByClassName('mutation-observer')[0];
    /**
     * mutationObserverCallbackInitial detects all element attribute changes on subtree of target element with class "mutation-observer"
     * mutationObserverCallbackInitial makes sure that updatePagesNodeListObserver also runs on navigation back.
     *
     * mutationObserverCallbackChildList detects only childList changes inside target element with class "mutation-observer" so it runs only when new paginated content component gets appended.
     */
    const mutationObserverCallbackChildList = function () {
      this.updatePagesNodeListObserver();
    }.bind(this);

    const mutationObserverCallbackInitial = function () {
      this.updatePagesNodeListObserver();

      // fetching cachedContent once updatePagesNodeListObserver is run and we have nextPage, prevPage
      this.$emit('fetchCached', {
        nextPage: this.nextPage,
        prevPage: this.prevPage,
      });

      /**
       * once mutation observer runs initialy, we disconnect it and intialize new one with only childList: true option, which only gets triggered when new page is fetched and new elemnt gets appended to childList.
       * this prevens mutation observer from running unnecessary, to improve performance.
       */
      this.mutationObserver.disconnect();

      this.mutationObserver = new MutationObserver(mutationObserverCallbackChildList);
      this.mutationObserver.observe(mutationObserverTargetEl, {
        childList: true,
      });
    }.bind(this);

    this.mutationObserver = new MutationObserver(mutationObserverCallbackInitial);
    this.mutationObserver.observe(mutationObserverTargetEl, {
      subtree: true,
      attributes: true,
    });
  }

  destroyObservers() {
    this.observer.disconnect();
    this.mutationObserver.disconnect();
  }

  updatePagesNodeListObserver() {
    // updating pages, nodeList and observer when new page is added/fetched
    this.pageItemsNodeList = document.querySelectorAll(`[data-target="${this.dataTarget}"]`);

    // first element of nodelist will always contain prev_page in dataset and when element is 1st dataset.prevpage returns undefined
    this.prevPage = +(this.pageItemsNodeList.item(0) as HTMLElement).dataset.prevpage;
    // last element of nodelist will always contain next_page in dataset and when element is last dataset.nextpage returns undefined
    this.nextPage = +(this.pageItemsNodeList.item(this.pageItemsNodeList.length - 1) as HTMLElement).dataset.nextpage;

    this.pageItemsNodeList.forEach((paginationNodeItem: HTMLElement) => {
      this.observer.observe(paginationNodeItem);
    });
  }

  fetchMoreContent(page, loadingType: 'prev' | 'next') {
    this.$emit('fetch', { page, loadingType, apiProp: this.apiProp, vuexModuleProp: this.vuexModulesForFetch });
  }

  fetchPrevContent(page, loadingType) {
    this.fetchMoreContent(page, loadingType);
    // page=1 should not exist
    const replaceUrl = `${this.$route.path}?page=${page}`.replace(/\?page=1\b/, '');
    // Making sure that we are not replacing same route
    if (replaceUrl !== this.$route.fullPath) this.$router.replace({ path: replaceUrl });
  }

  controlPagination(action: 'update' | 'destroy', observerCurrentPage: number = null) {
    if (action === 'update') {
      this.setPagination({
        isEnabled: true,
        currentPage: observerCurrentPage ? observerCurrentPage : this.currentPage,
        totalPages: this.totalPages,
      });
    } else {
      this.setPagination({
        isEnabled: false,
        currentPage: 0,
        totalPages: 0,
      });
    }
  }

  created() {
    this.currentPage = +this.$route.query.page || 1; // index page is considered as 1st page
    this.controlPagination('update');
  }

  mounted() {
    this.initiateUrlObserver();
    this.initiateMutationObserver();
  }

  beforeDestroy() {
    this.destroyObservers();
    this.controlPagination('destroy');
  }

  head() {
    return {
      title: `${this.metaData.title} ${this.$route.query.page ? `page - ${this.$route.query.page}` : ''}`,
      meta: [
        {
          hid: 'description',
          name: 'description',
          content: this.$truncate(this.metaData.description, {
            length: 160,
          }),
        },
      ],
      link: [
        ...generateLinkTagsData({
          urlPath: this.$route.path,
          currentPage: this.currentPage,
          totalPages: this.totalPages,
        }),
      ],
    };
  }
}
</script>
